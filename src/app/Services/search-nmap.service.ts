import { Injectable } from '@angular/core';
// declare var require: any;
import {ChildProcessService} from 'ngx-childprocess';
import { IDService } from './id.service';
import {xml2json} from 'xml-js';
import { Observable } from 'rxjs';
import { from } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class SearchNmapService {

  constructor(private ChildProccess: ChildProcessService,
    private IdCreator: IDService) { }

  public  SearchNmap(command):Promise<any> {
//  console.log('creating search promise');
return new Promise((resolve, reject)=>{
  // console.log('starting promise')
  let TmpFilePath = this.addWriteToFileExt();
  this.ChildProccess.childProcess.exec(command + ' -oX ' + TmpFilePath,[null],
  (err,UnDecodedCommandResult,code)=>{
    // console.log('done search and writing to file');
    // console.log(UnDecodedCommandResult)
    // let NmapTerminalResult = new TextDecoder('utf-8').decode(UnDecodedCommandResult);
    this.ReadFromTmpFile(TmpFilePath).then((NmapXmlFileResult)=>{
      // console.log(NmapXmlFileResult);
      // let JsonResult = this.convertToJson( new TextDecoder('utf-8').decode(NmapXmlFileResult));
      resolve(this.convertToJson(NmapXmlFileResult));
    });
  });
});

}

 private addWriteToFileExt() {
    return 'TmpFiles/' + this.IdCreator.CreateID() + '.xml';
 }

 private ReadFromTmpFile(path: string):Promise<any> {
   return new Promise((resolve,reject)=>{
   this.ChildProccess.childProcess.exec('cat ' + path, [null],(err,result,code)=>{
      this.ChildProccess.childProcess.exec("rm "+path,[null],null); 
      resolve(result)
    });
   });
}

 public convertToJson(data: string): string {
  return JSON.parse(xml2json(data, {compact: true, spaces: 4}));
}

}
