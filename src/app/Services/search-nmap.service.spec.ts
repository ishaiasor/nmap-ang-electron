import { TestBed } from '@angular/core/testing';

import { SearchNmapService } from './search-nmap.service';

describe('SearchNmapService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: SearchNmapService = TestBed.get(SearchNmapService);
    expect(service).toBeTruthy();
  });
});
