import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class IDService {

  constructor() { }

  public CreateID () {
    return '_' + Math.random().toString(36).substr(2, 9);
  };
}
