import { TestBed } from '@angular/core/testing';

import { GetNetworkCardsService } from './get-network-cards.service';

describe('GetNetworkCardsService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: GetNetworkCardsService = TestBed.get(GetNetworkCardsService);
    expect(service).toBeTruthy();
  });
});
