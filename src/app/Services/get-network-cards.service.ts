import { Injectable } from '@angular/core';
import {ChildProcessService} from 'ngx-childprocess';
import { NetworkAsapter } from '../Models/network-asapter';
import {xml2json} from 'xml-js';

@Injectable({
  providedIn: 'root'
})
export class GetNetworkCardsService {

  constructor(private ChildProccess: ChildProcessService) { }

  public GetAllNetworkAdapters():any{
    var result = this.ChildProccess.childProcess.execSync('lshw -class network -xml',null);
    return this.convertToJson( new TextDecoder('utf-8').decode(result));
    
  }
  public convertToJson(data: string): string {
    return JSON.parse(xml2json(data, {compact: true, spaces: 4}));
  }
}
