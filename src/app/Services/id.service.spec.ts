import { TestBed } from '@angular/core/testing';

import { IDService } from './id.service';

describe('IDService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: IDService = TestBed.get(IDService);
    expect(service).toBeTruthy();
  });
});
