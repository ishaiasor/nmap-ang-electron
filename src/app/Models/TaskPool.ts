import { Task } from "./Task";

export class TaskPool{
  
    public Tasks:Task[] = [];
    public ActiveTasks:Task[] = [];
    public Current ;
    public constructor(){
           this.init();
    }

  

    public AddTask(task:Task):void
    {
        this.Tasks.push(task);
    }

    private init(){
        setInterval(()=>{
            // while(true){

                this.ActiveTasks.forEach((task:Task)=>{
                    if(task.IsDone)
                    {
                          var index = this.ActiveTasks.indexOf(task);
                          this.ActiveTasks.splice(index,1)
                    }
                   
                })

                if(this.ActiveTasks.length < 8)
                {
                    var task = this.Tasks.pop();
                    if(task)
                    {
                    this.Current = task;    
                    this.ActiveTasks.push(task);  
                    task.Run();
                    }
                }
                
            // }
        },100);
       
    }

}