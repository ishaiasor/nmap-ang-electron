export class Task{
    private Task = null;
    public IsDone = false;
    public Name:string;
    public IsStart = false;
    public IsRunning = false;
    public IsAborted = false;
    private interval;

    constructor(func:any,name){
        this.Task = func;
        this.Name = name;
    }

    public async Run  (){
        this.interval = setTimeout(async()=>{
            this.IsRunning = true;
            this.IsStart = true;
           await this.Task();
            // console.log('task : '+this.Name+' is done')
            this.IsDone = true;
            this.IsRunning = false;
        },0);
     
    }

    public Stop(){
      this.IsAborted = true;
      this.IsDone = true;
      this.IsRunning = false;
      if(this.interval){
          clearTimeout(this.interval);
      }
    }

}