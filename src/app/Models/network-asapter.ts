export class NetworkAsapter {

    constructor(data){
        this.description = data['description'];
        this.product = data['product'];
        this.vendor = data['vendor'];
        this.logicalname = data['logicalname']['_text'];
        this.serial = data['serial'];

        this.ip = data['configuration']['setting'].find(e =>
            {
                if( e._attributes.id == 'ip')
                return e;
        });
        if(this.ip)
          this.ip = this.ip['_attributes']['value'];
        else 
          this.ip = 'disconnected';
        console.log(this.ip)
        this.wireless = data['configuration']['wireless'];
        this.link = data['configuration']['link'];
    }
    description:string;
    product:string;
    vendor;
    logicalname;  
    serial;
    ip;
    wireless; 
    link;
}
