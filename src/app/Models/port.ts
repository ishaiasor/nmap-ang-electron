export class port {

    constructor(data){
        this.protocol = data['_attributes']['protocol'];
        this.portid = data['_attributes']['portid'];
        this.state = data['state']['_attributes']['state'];
        this.reason = data['state']['_attributes']['reason'];
        this.reason_ttl = data['state']['_attributes']['reason_ttl'];
        this.service_conf = data['service']['_attributes']['conf'];
        this.method = data['service']['_attributes']['method'];
        this.service_conf = data['service']['_attributes']['name'];
        
    }
    service_conf:string;
    method:string;
    name:string;
    state:any;
    protocol:string;
    portid:number;
    reason:string;
    reason_ttl:string;

}
