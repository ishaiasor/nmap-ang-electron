import { Address } from './address';
import {Hostname} from './hostname';
import { port } from './port';

export class Host {
    endtime: any;
    starttime: any;
    address: Address[];
    hostname: Hostname;
    ports : port [] = [];
}
