import { Component, OnInit } from '@angular/core';
// import * as Search from "../../Search";
import { from } from 'rxjs';
import { SearchNmapService } from '../Services/search-nmap.service';
import { ChildProcessService } from 'ngx-childprocess';
import { Host } from '../Models/host';
import { Hostname } from '../Models/hostname';
import { port } from '../Models/port';
import { GetNetworkCardsService } from '../Services/get-network-cards.service';
import { NetworkAsapter } from '../Models/network-asapter';
import { Task } from '../Models/Task';
import { TaskPool } from '../Models/TaskPool';
@Component({
  selector: 'app-search-log',
  templateUrl: './search-log.component.html',
  styleUrls: ['./search-log.component.css']
})
export class SearchLogComponent implements OnInit {

  data: Host [] = [];
  networkAdapters:NetworkAsapter[] = [];
  selectedCard:NetworkAsapter = null;
  pool:TaskPool;  
  constructor(private SearchService: SearchNmapService, private networkservice:GetNetworkCardsService) { }

  ngOnInit() {
    this.pool = new TaskPool();
    var list = this.networkservice.GetAllNetworkAdapters()['list']['node'];
    list.forEach(e=>{this.networkAdapters.push(new NetworkAsapter(e))})
  }

   Search() {
    // new Promise((resolve,reject)=>{
      var ip = this.selectedCard.ip.split('.');
      for(var i =0 ;i<256;i++){
     this.searchHost(ip[0]+'.'+ip[1]+'.'+ip[2]+'.'+i);
      }
    // });
  }

  private async searchHost(hostIP){
    var name = 'nmap -e '+this.selectedCard.logicalname+' '+hostIP;
       var x = async()=>{ 
   await this.SearchService.SearchNmap('nmap '+hostIP)
   .then(result=>{
    const host: Host = new Host();
    // try
    //   {
        if(!result['nmaprun']['host'])
            return;
        console.log(result)
        try{
          host.hostname = result ['nmaprun']['host']['hostnames']['hostname']['_attributes'];
        }catch(e){}
        try{
          host.address = result ['nmaprun']['host']['address']['_attributes'];
        }catch(e){}
        try{
          var list  = result ['nmaprun']['host']['ports']['port'];
        list.forEach(element => 
          {
           host.ports.push(new port(element));
          });
          this.data.push(host);
        }catch(e){}
         
        
      
  // }
  // catch(e){console.log(e);console.log('ip = '+hostIP)}
});
   }
  
  var t = new Task(x,name);

  this.pool.AddTask(t);
 
    // t.Run();
    // console.log(t.IsDone);
  // x();
  
  }
}
