import { Component, OnInit,Input } from '@angular/core';
import {Host} from '../Models/host';

@Component({
  selector: 'app-host-row',
  templateUrl: './host-row.component.html',
  styleUrls: ['./host-row.component.css']
})
export class HostRowComponent implements OnInit {

  @Input() host: Host ;
  constructor() { }

  ngOnInit() {
  }

}
